define(['App', 'backbone', 'marionette', 'views/WelcomeView', 'views/DesktopHeaderView', 'views/LoginView'],
    function (App, Backbone, Marionette, WelcomeView, DesktopHeaderView, LoginView) {

        return Backbone.Marionette.Controller.extend({
            initialize:function (options) {
                App.headerRegion.show(new DesktopHeaderView());
            },
            //gets mapped to in AppRouter's appRoutes
            index: function () {
                // App.headerRegion.show(new DesktopHeaderView()); // show different header
                App.mainRegion.show(new WelcomeView());
            },

            login: function(){
                App.mainRegion.show(new LoginView());
            }
        });

    }
);